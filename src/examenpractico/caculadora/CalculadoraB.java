 
package examenpractico.caculadora;

/**
 *
 * @author Ivan
 */
public class CalculadoraB extends javax.swing.JFrame {
 
    public CalculadoraB() {
        initComponents();
    }
 static String numero1="";
 static String numero2="";
 static String ope="";
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JTFpantalla = new javax.swing.JTextField();
        Bsuma = new javax.swing.JButton();
        B1 = new javax.swing.JButton();
        B2 = new javax.swing.JButton();
        B3 = new javax.swing.JButton();
        B6 = new javax.swing.JButton();
        B5 = new javax.swing.JButton();
        B4 = new javax.swing.JButton();
        Bresta = new javax.swing.JButton();
        B9 = new javax.swing.JButton();
        B8 = new javax.swing.JButton();
        B7 = new javax.swing.JButton();
        Bmultipli = new javax.swing.JButton();
        Bclean = new javax.swing.JButton();
        B0 = new javax.swing.JButton();
        Bdecimal = new javax.swing.JButton();
        Bdivision = new javax.swing.JButton();
        Bequal = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        JTFpantalla.setText(" ");
        JTFpantalla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JTFpantallaActionPerformed(evt);
            }
        });

        Bsuma.setText("+");
        Bsuma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BsumaActionPerformed(evt);
            }
        });

        B1.setText("1");
        B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B1ActionPerformed(evt);
            }
        });

        B2.setText("2");
        B2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2ActionPerformed(evt);
            }
        });

        B3.setText("3");
        B3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B3ActionPerformed(evt);
            }
        });

        B6.setText("6");
        B6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B6ActionPerformed(evt);
            }
        });

        B5.setText("5");
        B5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B5ActionPerformed(evt);
            }
        });

        B4.setText("4");
        B4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B4ActionPerformed(evt);
            }
        });

        Bresta.setText("-");
        Bresta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BrestaActionPerformed(evt);
            }
        });

        B9.setText("9");
        B9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B9ActionPerformed(evt);
            }
        });

        B8.setText("8");
        B8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B8ActionPerformed(evt);
            }
        });

        B7.setText("7");
        B7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B7ActionPerformed(evt);
            }
        });

        Bmultipli.setText("*");
        Bmultipli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BmultipliActionPerformed(evt);
            }
        });

        Bclean.setText("C");
        Bclean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BcleanActionPerformed(evt);
            }
        });

        B0.setText("0");
        B0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B0ActionPerformed(evt);
            }
        });

        Bdecimal.setText(" .");
        Bdecimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BdecimalActionPerformed(evt);
            }
        });

        Bdivision.setText("/");
        Bdivision.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BdivisionActionPerformed(evt);
            }
        });

        Bequal.setText("=");
        Bequal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BequalActionPerformed(evt);
            }
        });

        jLabel1.setText("CALCULADORA BASICA");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addComponent(Bequal, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Bdivision, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(Bsuma, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                                                .addComponent(B1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(Bresta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(Bmultipli, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE))
                                                .addGap(28, 28, 28)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(B7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(Bdecimal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(B4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                    .addComponent(B8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                                                    .addComponent(B0, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(B5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addComponent(B2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(Bclean, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                                    .addComponent(B9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(B6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(B3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(JTFpantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JTFpantalla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B1)
                    .addComponent(Bsuma)
                    .addComponent(B2)
                    .addComponent(B3))
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B4)
                    .addComponent(Bresta)
                    .addComponent(B5)
                    .addComponent(B6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B7)
                    .addComponent(Bmultipli)
                    .addComponent(B8)
                    .addComponent(B9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Bdecimal)
                    .addComponent(Bdivision)
                    .addComponent(B0)
                    .addComponent(Bclean, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Bequal)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B1ActionPerformed
String boton = "1";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
    }//GEN-LAST:event_B1ActionPerformed

    private void B2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2ActionPerformed
   String boton = "2";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
    }//GEN-LAST:event_B2ActionPerformed

    private void B5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B5ActionPerformed
String boton = "5";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
    }//GEN-LAST:event_B5ActionPerformed

    private void B9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B9ActionPerformed
            String boton = "9";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);         
    }//GEN-LAST:event_B9ActionPerformed

    private void BcleanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BcleanActionPerformed
             String boton = "";
             numero1="";
             numero2="";
             ope="";
JTFpantalla.setText(boton);        
    }//GEN-LAST:event_BcleanActionPerformed

    private void BequalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BequalActionPerformed
String form = JTFpantalla.getText();
form.trim();
String num1,ope,num2;
int coorde;
 /*num1=decimal1(form);
 ope=num1.substring(num1.length()-1);
num1=num1.substring(0,num1.length()-2);
num2=decimal2(form);*/
 /*coorde=coorde(form);
  ope=form.substring(coorde);
 num1=form.substring(0,coorde-1);
 num2=num1.substring(coorde+1,form.length()-1);
 */
double uno = Double.parseDouble(numero1);
double dos = Double.parseDouble(numero2);
String R=""+resultado(uno,dos)+"";
JTFpantalla.setText(R);
numero2="";
numero1=R;
ope="";

    }//GEN-LAST:event_BequalActionPerformed

    private void JTFpantallaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JTFpantallaActionPerformed
      //JTFpantalla.setEditable(false);
    //  JTFpantalla.setEnabled(false);
    }//GEN-LAST:event_JTFpantallaActionPerformed

    private void B3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B3ActionPerformed
String boton = "3";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
 
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);        
    }//GEN-LAST:event_B3ActionPerformed

    private void B4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B4ActionPerformed
 String boton = "4";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
    }//GEN-LAST:event_B4ActionPerformed

    private void B6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B6ActionPerformed
String boton = "6";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
    }//GEN-LAST:event_B6ActionPerformed

    private void B7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B7ActionPerformed
String boton = "7";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
    }//GEN-LAST:event_B7ActionPerformed

    private void B8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B8ActionPerformed
String boton = "8";
  if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
    }//GEN-LAST:event_B8ActionPerformed

    private void B0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B0ActionPerformed
String boton = "0";
 if(numero1.equals("")&&ope.equals("")){
    numero1+=boton;
}else if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&numero2.equals("")) {
     numero2+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
    }//GEN-LAST:event_B0ActionPerformed

    private void BsumaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BsumaActionPerformed
if(Comprobar(JTFpantalla.getText())==true){//evitar errore
String boton = "+";
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
ope=boton;
}



    }//GEN-LAST:event_BsumaActionPerformed

    private void BrestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BrestaActionPerformed
if(Comprobar(JTFpantalla.getText())==true){//evitar errore
String boton = "-";
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
ope=boton;
}
    }//GEN-LAST:event_BrestaActionPerformed

    private void BmultipliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BmultipliActionPerformed
if(Comprobar(JTFpantalla.getText())==true){//evitar errore
String boton = "*";
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
ope=boton;
}
        
    }//GEN-LAST:event_BmultipliActionPerformed

    private void BdivisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BdivisionActionPerformed
if(Comprobar(JTFpantalla.getText())==true){//evitar errore
String boton = "/";
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);
ope=boton;
}
    }//GEN-LAST:event_BdivisionActionPerformed

    private void BdecimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BdecimalActionPerformed
if(Comprobar(JTFpantalla.getText())==true){//evitar errore
String boton = ".";
  if(!numero1.equals("")&&ope.equals("")&&numero2.equals("")){
   numero1+=boton;
} else if(!numero1.equals("")&&!ope.equals("")&&!numero2.equals("")){
    numero2+=boton;
}
String contenido = JTFpantalla.getText();
contenido = contenido+boton;
JTFpantalla.setText(contenido);

}
    }//GEN-LAST:event_BdecimalActionPerformed
 
    
    public String Obtener_ultimo(String formula){ 
        String ultimochar = formula.substring(formula.length()-1); 
        char[] arrayChar = ultimochar.toCharArray(); 
        //arrayChar[arrayChar.length - 1]
        String ultimo = new StringBuilder().append("").append(arrayChar[arrayChar.length - 1]).toString(); 
   return ultimo.trim();     
 }
    
  
 public int coorde(String decimales){
     decimales.trim();
     String reco;
    int coorde=0;
    int i=0;
    while(i<decimales.length()){    
        reco=decimales.substring(i);
        if(reco.equals("+")||reco.equals("-")||reco.equals("*")||reco.equals("/")){
         // if((decimales.substring(i).equals("+"))||(decimales.substring(i).equals("-"))||(decimales.substring(i).equals("*"))||(decimales.substring(i).equals("/"))){
            coorde=i;
            return coorde;
          }
         i++;
     }
    
    return coorde;
 }
 
 public double resultado(double n1,double n2){
     double r=0;
     switch(ope){
         case "+":{
             r = n1+n2;
             return r;
         } 
         case "-":{
             r  = n1-n2;
             return r;
         } 
         case "*":{
             r  = n1*n2;
             return r;
         } 
         case "/":{
             r  = n1/n2;
             return r;
         } 
     }
     
     return r;
 }
 
  
 
 
 
 
 
 
 
 
 
 public boolean Comprobar(String form){
    if(form.equals("")){
        return false;
    }
   form =Obtener_ultimo(form); 
     if(form.equals("+")||form.equals("/")||form.equals("*")||form.equals("-")||form.equals(".")){
         return false;
     }     
     
     return true;
 } /*
 public void Jerarquia(String formula){ 
     char[] operandos;
     char[] numeros;
     String resultado;
     
     String formulachar = formula.substring(0,formula.length()-1); 
     formulachar.trim(); 
        char[] bruta = formulachar.toCharArray();  
        
     for(int i=0; i<bruta.length; i++){//guardar num
         
     } 
 }*/
 
 
   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CalculadoraB.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CalculadoraB.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CalculadoraB.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CalculadoraB.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CalculadoraB().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B0;
    private javax.swing.JButton B1;
    private javax.swing.JButton B2;
    private javax.swing.JButton B3;
    private javax.swing.JButton B4;
    private javax.swing.JButton B5;
    private javax.swing.JButton B6;
    private javax.swing.JButton B7;
    private javax.swing.JButton B8;
    private javax.swing.JButton B9;
    private javax.swing.JButton Bclean;
    private javax.swing.JButton Bdecimal;
    private javax.swing.JButton Bdivision;
    private javax.swing.JButton Bequal;
    private javax.swing.JButton Bmultipli;
    private javax.swing.JButton Bresta;
    private javax.swing.JButton Bsuma;
    private javax.swing.JTextField JTFpantalla;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
