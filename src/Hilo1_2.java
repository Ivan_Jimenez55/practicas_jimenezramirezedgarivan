
import java.util.logging.Level;
import java.util.logging.Logger;

 
public class Hilo1_2 {
     public static void main(String[] args){
        HiloT h1 = new HiloT("P1",10,2000);
       Thread h2 = new Thread(new HiloR("P2",20,1500));    
        
        h1.start();
        h2.start();
        
        System.out.println("Proceso main terminado");
    }
}


class HiloT extends Thread{
    int x,tiempo ;
    String id;
    
    
    HiloT(String id, int x, int tiempo){
       this.x = x;
       this.tiempo = tiempo;
       this.id=id;
    }
  
    public void run(){
        for(int i=0; i<x; i++){
        System.out.printf("Hilo: %s - i =%d\n",id,i);
        try{
            Thread.sleep(this.tiempo);
        }catch(InterruptedException ex) {
            Logger.getLogger(HiloT.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    }
}
class HiloR implements Runnable {
    int x,tiempo ;
    String id;
    
    
    HiloR(String id, int x, int tiempo){
       this.x = x;
       this.tiempo = tiempo;
       this.id=id;
    }
  
    public void run(){
        for(int i=0; i<x; i++){
        System.out.printf("Hilo: %s - i =%d\n",id,i);
        try{
            Thread.sleep(this.tiempo);
        }catch(InterruptedException ex) {
            Logger.getLogger(HiloR.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    }
}
