 
package Chatjframe;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
 
public class ServidorJframe {
    ArrayList<Conexion> conexiones;//se declara el array de conexiones(HILOS)
    ServerSocket   ss;
    
    String [][] usuarios = {{"hugo",  "123"},
                            {"paco",  "345"},
                            {"luis",  "890"},
                            {"carlos","678"}};
    String [][] entrada = {{"",""}};
    String conectados [] = new String[4]; 
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new ServidorJframe()).start();
            }
        });
        /*
        1 - paco
        2 - carlos
        11101111101
        1 - paco
        2 - carlos
        11101111101
        1 - paco
        2 - carlos
        11101111101
        1 - paco
        2 - carlos
        */
} 
public  void start() {
        this.conexiones = new ArrayList<Conexion>(); //se inicializa/instancia
        Socket socket;//se crea un objeto socket
        
        Conexion cnx;
        try {
            ss = new ServerSocket(4444);//se declara el numero de puerto del socket
            System.out.println("Servidor iniciado, en espera de conexiones");
            
            while (true){       //el servidor se mantiene en este estado aceptando conexiones      
                socket = ss.accept();//entrada de conexion aceptada
                cnx = new Conexion(this, socket, conexiones.size());//se agrega una nueva conexion con el objeto de la claseinterna 
                conexiones.add(cnx);
                cnx.start(); //se abre otro hilo con la conexion nueva                
            }    
        } catch (IOException ex) {
            Logger.getLogger(ServidorJframe.class.getName()).log(Level.SEVERE, null, ex);
        }            
    } 

  public void difundir(String id, String mensaje) {//metodo con el cualese envian mensajes a todos los usuarios conectdos
        Conexion hilo;
        for (int i = 0; i < this.conexiones.size(); i++){//for para recorrer el array de hilos/objetos
            hilo = this.conexiones.get(i);//se obtiene el objeto dentro del array
            
            if (hilo.cnx.isConnected()){//se comprueba que la conexion este conectada 
                    hilo.enviar(mensaje); 
            }
        }//To change body of generated methods, choose Tools | Templates.
    }
  
 public void enviarCONECTADOS(String id, String bin){//se recibe el id de la persona que pidio la actualizacion
            Conexion usuarios;//se hace un objeto para recorrer las lista de conexiones
            String inf= "USUARIOS CONECTADOS\n";
            int cantidad=1;
            for (int i = 0; i < this.conexiones.size(); i++){ //for para recorrer el array de hilos/objetos)
                usuarios = this.conexiones.get(i);//se obtiene la conexion    
                if(usuarios.cnx.isConnected()){//se verifica si el usuario esta conectado
                    inf += cantidad+" - "+usuarios.nombre_usuario+"\n";//se agrega el nombredel usuario en el mensaje
                    cantidad++;
                }
            }
            for (int j = 0; j < this.conexiones.size(); j++){//se busca denuevo a la conexion que pidio la actualizacion
                 usuarios = this.conexiones.get(j);//se obtiene el objeto dentro del array 
                 if (id.equalsIgnoreCase(usuarios.id)){
                    usuarios.enviarinf(inf); 
                 }
            }
            
        }
 
  class Conexion extends Thread {//PROCESO CON EL QUE MANTIENE CONECTADO
      
        BufferedReader in;//OBJETO CON EL QUE RECIBE Strings
        PrintWriter    out;// OBJETO CON EL QUE ENVIA strings
        Socket cnx; //SOCKET CON EL CUAL SE CONECTA
        String nombre_usuario="";
        ServidorJframe padre;
        int numCnx = -1;
        String id = "";
        //atributos que definen la situacion para actuarcon el lenguaje automata
        public final int SIN_USER   = 0;
        public final int USER_IDENT = 1;//PARA INDENTIFICAR AL USUARIO
        public final int PASS_PDTE  = 2;//PARA 
        public final int PASS_OK    = 3;//LOS DATOS SON CORRECTOS
        public final int CHAT       = 4;//PROCESO GENERAL DE CHAT
                
        public Conexion(ServidorJframe padre, Socket socket, int num){//el constructor recibe el objeto padre, socker y el numero de
            this.cnx = socket;//objeto de la conexion
            this.padre = padre;//socket padre
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+num;//el ide de la conexionSOCKET es su ip y el numero de puerto 
            
        }
      
        @Override
        public void run() {
            String linea="", user="", pass="", mensaje="";
            int estado = SIN_USER;
            int usr = -1;
            
            try {
                //OBJETO CON EL QUE SE RECIBE LA RECEPCION DE INFORMACION DEL EXTERIOR
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                //OBJETO CON EL QUE SE ENVIA LA INFORMACION AL CLIENTE AL EXTERIOR
                out = new PrintWriter(cnx.getOutputStream(),true);
                //SE IMPRIME QUIEN SE CONECTA 
                System.out.printf("Aceptando conexion desde %s\n",
                        cnx.getInetAddress().getHostAddress());
                
                while(!mensaje.toLowerCase().equals("salir")){                    
                    switch (estado){
                         case SIN_USER://se inicia aqui porque el usuario aun no se ha identificado
                            out.println("Bienvenido, proporcione su usuario:");
                            estado = USER_IDENT;
                            break;
                        
                        
                         case USER_IDENT://SE IDENTIFICA AL USUARIO
                            user = in.readLine();//SE LEE LO QUE EL USUARIO HAYA INGRESADO
                            boolean found = false;//BOOLEANO PARA COMBROBAR SI ESE USUARIO EXISTE
                            for (int i=0; i < usuarios.length; i++){ //SE RECORRE EL ARRAY DE CONEXIONES PARA BUSCAR LA ID IGUAL
                                if (user.equalsIgnoreCase(usuarios[i][0])){ 
                                   out.println("USUARIO ENCONTRADO: "+user);
                                    found = true;
                                    usr = i; 
                                }
                            }
                            if (!found){//SI NO SE ENCONTRO EL USUARIO, ENTONCES SE MANTIENE EN EL MISMO ESTADO
                                out.println(" USUARIO NO ENCONTRADO,\n"
                                        + " INTENTE INGRESANDO UNO EXISTENTE");
                                estado = USER_IDENT;
                            } else {
                                estado = PASS_PDTE;//
                                
                            }                                                                                                             
                            break;
                            
             //----------------------------------------------------               
                        case PASS_PDTE://estado para leer la contraseña
                             out.println("Escriba el password: ");
                            pass = in.readLine(); //SE RECIBE el string de LA CONTRASEÑA 
                            if (pass.equalsIgnoreCase(usuarios[usr][1])){//se comprueba si la contraseña concuerda
                               out.println("CONTRASEÑA CORRECTA"); 
                                estado = PASS_OK; 
                            }else{
                              out.println("CONTRASEÑA CORRECTA, INGRESELA DE NUEVO");  
                            }
                            break;
             //--------------------------------------------------------               
                        case PASS_OK:
                            nombre_usuario=user;
                            this.padre.enviarCONECTADOS(this.id,"@11101111101");
                            out.println("  ¡AUTENTIFICADO!\n"+
                                        "   ¡DI HOLA!"); 
                            estado = CHAT;//SE CAMBIA EL ESTADO A CHAT
                            break;
             //--------------------------------------------------------      
                        case CHAT:
                                mensaje = in.readLine();//SE RECIBEN LOS MENSAJES QUE ENVIE EL CLIENTE 
                               if(mensaje.length()<=12 && mensaje.equals("@11101111101") ){
                                    this.padre.enviarCONECTADOS(this.id,"@11101111101");
                                }else{
                                 System.out.printf("%s - %s\n",//SE ESCRIBE EL MENSAJE RECIBIDO DEL EXTERIOR Y SE IMPRIME EN LA CONSOLA POR CUALQUIER COSA
                                        cnx.getInetAddress().getHostAddress(),
                                        user + ":" +mensaje);
                               //SE LLAMA AL METODO DIFUNDIR PARA ENVIAR EL MENSAJE A TODOS
                                this.padre.difundir(this.id, user+": "+mensaje);
                                break;    
                                }      
                               
                               
                    }                        
                }                
                this.cnx.close();
            } catch (IOException ex) {
                Logger.getLogger(ServidorJframe.class.getName()).log(Level.SEVERE, null, ex);
            } catch(Exception e){
                System.out.println(e.getMessage());
            } 
          
        } 
         private void enviarinf(String mensaje) {
          //  out.print(mensaje); //SE ENVIA AL EXTERIOR EL MENSAJE EN BUSQUEDA DE SER RECIBIDO POR EL HILO RECEPTOR DEL CLIENTE
            out.println(mensaje); 
        } 
        private void enviar(String mensaje) {
            out.println(mensaje); //SE ENVIA AL EXTERIOR EL MENSAJE EN BUSQUEDA DE SER RECIBIDO POR EL HILO RECEPTOR DEL CLIENTE
        } 
    }     
}
