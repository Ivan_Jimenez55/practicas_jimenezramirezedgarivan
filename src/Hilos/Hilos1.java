
package Hilos;
 
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.util.logging.PlatformLogger;

public class Hilos1 {
    public static void main(String[] args){
        Hilo h1 = new Hilo("P1",10,2000);
        Hilo h2 = new Hilo("P2",20,1500);
        Hilo h3 = new Hilo("P3",30,1000);       
        
        h1.start();
        h2.start();
        h3.start();
        System.out.println("Proceso main terminado");
    }
}
class Hilo extends Thread{
    int x,tiempo ;
    String id;
    
    
    Hilo(String id, int x, int tiempo){
       this.x = x;
       this.tiempo = tiempo;
       this.id=id;
    }
  
    public void run(){
        for(int i=0; i<x; i++){
        System.out.printf("Hilo: %s - i =%d\n",id,i);
        try{
            Thread.sleep(this.tiempo);
        }catch(InterruptedException ex) {
            Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    }
}