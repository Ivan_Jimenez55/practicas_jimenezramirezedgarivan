/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chat;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author ihvan
 */
public class Servidor {
    ArrayList<Conexion> conexiones;//conexiones al servidor
    ServerSocket   ss; //objeto del socket
    String [][] online;
    String [][] usuarios = {{"hugo",  "123"},//USUARIOS POSIBLES
                            {"paco",  "345"},
                            {"luis",  "890"},
                            {"donald","678"}};
    
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor()).start();
            }
        });
    }

    private void start() {
        this.conexiones = new ArrayList<>();
        Socket socket;
        Conexion cnx;
        
        try {
            ss = new ServerSocket(4444);//numero del servidor socket
            System.out.println("Servidor iniciado, en espera de conexiones");
            
            while (true){             
                socket = ss.accept();//espera conexiones para aceptarlas, cada conexion nueva la agrega a un arreglo
                cnx = new Conexion(this, socket, conexiones.size());
                conexiones.add(cnx);
                cnx.start();                
            }            
                       
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }            
    }
    
    // Broadcasting
    private void difundir(String id, String mensaje) {//el mensaje que llegue se difunde a todoslos clientes conectados
        Conexion hilo; //hilo (objeto) donde estan albergadas las conexiones de los clientes
        for (int i = 0; i < this.conexiones.size(); i++){
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected()){//se verifica que conexiones estan conectadas para enviar el mensaje
                if (!id.equals(hilo.id)){//aqui se verifica que lapersona que envio un mensaje no reciba el miso mensaje que envio
                    hilo.enviar(mensaje);
                }
            }
        }//To change body of generated methods, choose Tools | Templates.
    }
    
    class Conexion extends Thread {
        BufferedReader in;
        PrintWriter    out;        
        Socket cnx;
        Servidor padre;
        int numCnx = -1;
        String id = "";
        
        public final int SIN_USER   = 0;//variables del tipo estado (LENUAJE AUTOMATA)
        public final int USER_IDENT = 1;
        public final int PASS_PDTE  = 2;
        public final int PASS_OK    = 3;
        public final int CHAT       = 4;
                
        public Conexion(Servidor padre, Socket socket, int num){
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+num;
        }
       
        @Override
        public void run() {
            String linea="", user="", pass="", mensaje="";
            int estado = SIN_USER;
            int usr = -1;
            
            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(),true);
                
                System.out.printf("Aceptando conexion desde %s\n",
                        cnx.getInetAddress().getHostAddress());
                                
                while(!mensaje.toLowerCase().equals("salir")){                    
                    switch (estado){
                        
                        case SIN_USER:
                            out.println("Bienvenido, proporcione su usuario");
                            estado = USER_IDENT;
                            break;
                 //----------------------------------------------------           
                        case USER_IDENT:
                            user = in.readLine();
                            boolean found = false;
                            for (int i=0; i < usuarios.length; i++){
                                if (user.equals(usuarios[i][0])){
                                    found = true;
                                    usr = i;
                                }
                            }       
                            if (!found){
                                estado = SIN_USER;
                                
                            } else {
                                estado = PASS_PDTE;
                            }                                                                                                             
                            break;
                 //----------------------------------------------------           
                        case PASS_PDTE:
                            out.println("Escriba el password");
                            pass = in.readLine();                            
                            if (pass.equals(usuarios[usr][1])){
                                estado = PASS_OK;
                            }
                            break;
                            
                 //----------------------------------------------------
                        case PASS_OK:
                            out.println("Autenticado!");
                            estado = CHAT;
                            break;
                        case CHAT:
                            mensaje = in.readLine();//se reciben mensajes recibidos del exterior
                            System.out.printf("%s - %s\n",
                                        cnx.getInetAddress().getHostAddress(),
                                        user + ":" +mensaje);
                            
                            this.padre.difundir(this.id, user+" : "+mensaje);
                            break;
                    }                        
                }                
                this.cnx.close();
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }

        private void enviar(String mensaje) {
            this.out.println(mensaje); //To change body of generated methods, choose Tools | Templates.
        } 
    }   
}
